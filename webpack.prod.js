const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const cm_module = require('./webpack.module.js');
module.exports = merge(common, cm_module, {
    devtool: 'source-map',
    mode: 'production',
});