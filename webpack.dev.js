const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const cm_module = require('./webpack.module.js');
module.exports = merge(common, cm_module, {
    devtool: 'inline-source-map',
    mode: 'development',
    devServer: {
        port: 118,
        progress: true, // 打包过程中的打包进度条
        contentBase: './dist', // 以./dist目录作为静态服务文件夹
        publicPath: '/dist' //网站运行时的访问路径。
    },
});