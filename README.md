-S, --save: Package will appear in your dependencies.
-D, --save-dev: Package will appear in your devDependencies.
https://www.webpackjs.com/guides/output-management/
1.  安装包
cnpm i webpack webpack-cli webpack-merge -D
cnpm i html-webpack-plugin clean-webpack-plugin -D
cnpm i style-loader css-loader file-loader csv-loader xml-loader lodash-cli  -D
cnpm i typescript ts-loader html-loader -D

2.  编译 cnpm run build
3.  本地运行 cnpm run dev


https://github.com/senntyou/lila/tree/master/packages/create-lila-app
https://github.com/PanJiaChen/vue-element-admin
https://blog.csdn.net/weixin_34401479/article/details/91438197?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3.channel_param

多页面应用
|-- src/ 源代码目录
 
    |-- page1/ page1 页面的工作空间（与这个页面相关的文件都放在这个目录下）
        |-- index.html html 入口文件
        |-- index.js js 入口文件
        |-- index.(css|less|scss) 样式入口文件
        |-- html/ html 片段目录
        |-- (css|less|scss)/ 样式文件目录
        |-- mock/ 本地 json 数据模拟
        |-- images/ 图片文件目录
        |-- components/ 组件目录（如果基于 react, vue 等组件化框架）
        |-- ...
        
    |-- sub-dir/ 子目录
        |-- page2/ page2 页面的工作空间（内部结构参考 page1）
            |-- ...
        
    |-- ...
    
|-- html/ 公共 html 片段
|-- less/ 公共 less 目录
|-- components/ 公共组件目录
|-- images/ 公共图片目录
|-- mock/ 公共 api-mock 文件目录

单页面应用
|-- src/ 源代码目录
 
    |-- page1/ page1 页面的工作空间（与这个页面相关的文件都放在这个目录下）
        |-- index.html html 入口文件
        |-- index.js js 入口文件
        |-- index.(css|less|scss) 样式入口文件
        |-- html/ html 片段目录
        |-- (css|less|scss)/ 样式文件目录
        |-- mock/ 本地 json 数据模拟
        |-- images/ 图片文件目录
        |-- components/ 组件目录（如果基于 react, vue 等组件化框架）
        |-- ...
        
    |-- sub-dir/ 子目录
        |-- page2/ page2 页面的工作空间（内部结构参考 page1）
            |-- ...
        
    |-- ...
    
|-- html/ 公共 html 片段
|-- less/ 公共 less 目录
|-- components/ 公共组件目录
|-- images/ 公共图片目录
|-- mock/ 公共 api-mock 文件目录

搭建一个好的脚手架
|-- /                              项目根目录
    |-- src/                       源代码目录
    |-- package.json               npm 项目文件
    |-- README.md                  项目说明文件
    |-- CHANGELOG.md               版本更新记录
    |-- .gitignore                 git 忽略配置文件
    |-- .editorconfig              编辑器配置文件
    |-- .npmrc                     npm 配置文件
    |-- .npmignore                 npm 忽略配置文件
    |-- .eslintrc                  eslint 配置文件
    |-- .eslintignore              eslint 忽略配置文件
    |-- .stylelintrc               stylelint 配置文件
    |-- .stylelintignore           stylelint 忽略配置文件
    |-- .prettierrc                prettier 配置文件
    |-- .prettierignore            prettier 忽略配置文件
    
    |-- .babelrc                   babel 配置文件
    |-- webpack.config.js          webpack 配置文件
    |-- rollup.config.js           rollup 配置文件
    |-- gulpfile.js                gulp 配置文件
    
    |-- test/                      测试目录
    |-- docs/                      文档目录
    |-- jest.config.js             jest 配置文件
    |-- .gitattributes             git 属性配置

收集前端错误反馈
当用户在用线上的程序时，怎么知道有没有出 bug；如果出 bug 了，报的是什么错；如果是 js 报错，怎么知道是那一行运行出了错？

所以，在程序运行时捕捉 js 脚本错误，并上报到服务器，是非常有必要的。

这里就要用到 window.onerror 了：

window.onerror = (errorMessage, scriptURI, lineNumber, columnNumber, errorObj) => {
  const data = {
    title: document.getElementsByTagName('title')[0].innerText,
    errorMessage,
    scriptURI,
    lineNumber,
    columnNumber,
    detailMessage: (errorObj && errorObj.message) || '',
    stack: (errorObj && errorObj.stack) || '',
    userAgent: window.navigator.userAgent,
    locationHref: window.location.href,
    cookie: window.document.cookie,
  };
 
  post('url', data); // 上报到服务器
};
