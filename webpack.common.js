const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const dirVars = require('./config/dir-vars.config.js'); // 与业务代码共用同一份路径的配置表
const webpack = require('webpack');

module.exports = {
    entry: {
        index: ['./src/index.js', './src/print.js'],
        // 'index/login': path.resolve(dirVars.pagesDir, `./index/login/page`),
        'index/login': path.resolve(dirVars.pagesDir, `./index/page`),
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[chunkhash].js',
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            // template: './src/index.html',
            filename: 'index.html',
            chunks: ['commons', 'index'],
            title: 'CMee,GO,GO,GO',
            template: 'src/template/template.ejs',
            minify: {
                collapseWhitespace: false, //删除空格、换行
            },
        }),
        new HtmlWebpackPlugin({
            filename: 'another.html',
            chunks: ['commons', 'another'],
            minify: {
                collapseWhitespace: false, //删除空格、换行
            },
        }),
    ],
    optimization: {
        //抽取公共的dm
        splitChunks: {
            cacheGroups: {
                chunks: 'all',
                commons: {
                    name: "commons",
                    chunks: "initial",
                    minChunks: 2
                }
            }
        }
    },
}